(TeX-add-style-hook
 "warning"
 (lambda ()
   (TeX-run-style-hooks
    "tcolorbox")
   (LaTeX-add-environments
    "info"
    "tip"
    "danger"))
 :latex)

