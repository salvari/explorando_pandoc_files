function Meta(m)
    m.subject = 'Flisol 2020, testing Lua Filters'
    m.date = os.date("%B %e, %Y")
    return m
end