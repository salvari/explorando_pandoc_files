function Strong(elem)
  return pandoc.SmallCaps(elem.content)
end

function Meta(m)
  m.date = os.date("%B %e, %Y")
  return m
end