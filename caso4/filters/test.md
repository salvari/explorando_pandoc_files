# Negrita

Esto es un texto con **negrita**.

::: {.warning}
Esto es un aviso
:::


::: {.info}
Esto es info
:::

::: {.tip}
Esto es una pista
:::

::: {.danger}
Esto es ¡un peligro!
:::
